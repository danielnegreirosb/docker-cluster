#!/bin/bash

if [[ $(hostname -s) = *master* ]]; then

  ## init swarm
  sudo docker swarm init --advertise-addr $1
  sudo docker swarm join-token worker  | grep token > /tmp/swarm-token
  chmod a+wrx /tmp/swarm-token
  sudo chown vagrant /tmp/swarm-token

	## Allow workers to perform ssh on the master host to fetch join command
	su - vagrant -c "echo \"ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDebBanpsJE2nh63sFRyHK8w0lK50CE1lY7jn2r4qwXQjsI/b3JIfIcnCy+ZGLs51AAvwpBG7JWyYDZtipfHDJqAkI4v6hcduHyGCOPi6K7HkhGIJxHy6n6yEjCWllUlPVyEqnhQZBtb1gBbWni/9UcwWWnyRCLZakFp1MwWfOK8K3YyEx41whoa8gqAruyimaLHOfes/GnDOY84e7szu/QZUeONKKvQwvX7NRuncrKgtZqyEACcwfrCUBBcfaQaCxXpB7p4s0BIpdEXYEZsJw4RRS/mWWpBD23p29NCUXsc09hf1nH0L0VfSt3u32y0cYNC1HQtOiSNku6Yy05foaB vagrant@worker01\" >> ~/.ssh/authorized_keys"

  ## Transfer keys to workers. With this key it can access master passwordless with vagrant user
	su - vagrant -c "cp /vagrant/scripts/id_rsa /home/vagrant/.ssh/"
	su - vagrant -c "cp /vagrant/scripts/id_rsa.pub /home/vagrant/.ssh/"
	chmod 400 /home/vagrant/.ssh/*

fi