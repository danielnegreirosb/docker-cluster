#!/bin/bash
sudo -i 

## Set Vagrant passoword and allow password ssh
echo "test123" | passwd --stdin vagrant
sed -i 's/#PasswordAuthentication/PasswordAuthentication/g' /etc/ssh/sshd_config
sudo systemctl restart sshd

## set hosts
cat << EOF >> /etc/hosts
192.168.60.10 docker-master
192.168.60.20 docker-worker01
192.168.60.30 docker-worker02
EOF

## Set dns
cat > /etc/resolv.conf <<EOF
nameserver 8.8.8.8
EOF

## set NTP
timedatectl set-timezone America/Sao_Paulo

## Install Docker and helper packages
sudo dnf install -y yum-utils
yum update -y

sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum -y install docker-ce docker-ce-cli containerd.io vim curl dos2unix wget

## Enable and start docker
systemctl start docker && systemctl enable docker

## Set basic Iptables
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 8000 -j ACCEPT
iptables -A INPUT -p tcp --dport 8080 -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT
iptables -A INPUT -p tcp --dport 4443 -j ACCEPT
iptables -A INPUT -p tcp --dport 6443 -j ACCEPT
iptables -A INPUT -p tcp --dport 8443 -j ACCEPT
iptables -A INPUT -p tcp --dport 6783 -j ACCEPT
iptables -A INPUT -p tcp --dport 2376 -j ACCEPT
iptables -A INPUT -p tcp --dport 2379 -j ACCEPT
iptables -A INPUT -p tcp --dport 2377 -j ACCEPT
iptables -A INPUT -p tcp --dport 7946 -j ACCEPT
iptables -A INPUT -p udp --dport 7946 -j ACCEPT
iptables -A INPUT -p tcp --dport 4789 -j ACCEPT
iptables -A INPUT -p udp --dport 4789 -j ACCEPT
iptables -A INPUT -p tcp --dport 10250 -j ACCEPT
iptables -A INPUT -p tcp --dport 10251 -j ACCEPT
iptables -A INPUT -p tcp --dport 10252 -j ACCEPT
iptables -A INPUT -p tcp --dport 30000:32767 -j ACCEPT
iptables-save

## Disable swap (Kubernetes requirement)
swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

## Disable SElinux
setenforce 0
sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux

## Set br_netfilter
modprobe br_netfilter
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables

## Change cgroupdriver to systemd and restart service
touch /etc/docker/daemon.json
cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "storage-driver": "overlay2"
}
EOF

systemctl restart docker

## Set Banner
cat << EOF > /etc/profile.d/banner.sh
clear
echo "|----------------------------------------------------------------------|"
echo "|   _____             _                _____ _           _             |"
echo "|  |  __ \           | |              / ____| |         | |            |"
echo "|  | |  | | ___   ___| | _____ _ __  | |    | |_   _ ___| |_ ___ _ __  |"
echo "|  | |  | |/ _ \ / __| |/ / _ \ '__| | |    | | | | / __| __/ _ \ '__| |"
echo "|  | |__| | (_) | (__|   <  __/ |    | |____| | |_| \__ \ ||  __/ |    |"
echo "|  |_____/ \___/ \___|_|\_\___|_|     \_____|_|\__,_|___/\__\___|_|    |"
echo "|                                                                      |"
echo "|-----------------------------------------------------------------------"
EOF

## Fix Docker permissions
su - vagrant -c "echo 'sudo chmod 0666 /var/run/docker.sock' >> ~/.bashrc"
su - vagrant -c "sudo usermod -aG docker vagrant"

## ssh no host check
su - vagrant -c "echo 'alias ssh=\"ssh -o StrictHostKeyChecking=no\"' >> ~/.bashrc"
su - vagrant -c "echo 'alias scp=\"scp -o StrictHostKeyChecking=no\"' >> ~/.bashrc"