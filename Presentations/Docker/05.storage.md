# Storage

If you command docker volume ls, we are going to see no volumes.
```
$ docker volume ls
DRIVER              VOLUME NAME
```

Lets create our volume my-data
```
$docker volume create my-data
my-data

$docker volume ls
DRIVER              VOLUME NAME
local               my-data
```

Now let's inspect this volume

```
$ docker inspect my-data
[
    {
        "CreatedAt": "2020-10-14T16:31:51Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/my-data/_data",
        "Name": "my-data",
        "Options": {},
        "Scope": "local"
    }
]
```

We can see that is created this directory for the my-data volume.
Let's check the content, it must be empry
```
$ ls /var/lib/docker/volumes/my-data/_data
```

Let's create a random file
```
$ echo "I was created by the docker hosts" > /var/lib/docker/volumes/my-data/_data/file-1
```

Ok now let's create a container using this volume
```
$ docker run -d --name web  --mount source=my-data,target=/my-mount  httpd
```

Let's check whats inside the my-mount direcoty.
And also check content
```
$ docker exec -it web  ls /my-mount
file-1

$ docker exec -it web  cat /my-mount/file-1
I was created by the docker hosts
```

Now we create a file from the inside of the container

```
$ docker exec -it web /bin/bash 
root@28cf66daf294:/usr/local/apache2# touch /my-mount/container-file
root@28cf66daf294:/usr/local/apache2# echo "Im inside the container" > /my-mount/container-file
root@28cf66daf294:/usr/local/apache2# exit
exit
```

Now let's check it on out host.
```
$ cat /var/lib/docker/volumes/my-data/_data/container-file
Im inside the container
```

Let's delete the container
```
$ docker stop web
web

$ docker rm web
web
```

Now let's check again on out host.
```
$ cat /var/lib/docker/volumes/my-data/_data/container-file
Im inside the container
```

Lets create a totally new container, and mount this same volume.
but changing the volumeMount.

Ok now let's create a container using this volume
```
$ docker run -d --name my-new-web  --mount source=my-data,target=/my-brand-new-mount  httpd
```

And there you see the files.

```
$ docker exec my-new-web  ls /my-brand-new-mount
container-file
file-1
```

Now let's delete container and volume

```
$ docker stop my-new-web
my-new-web

$ docker rm my-new-web
my-new-web

$ docker volume rm my-data
my-data
```

Check if local files still exists. It should be gone
```
$ cat /var/lib/docker/volumes/my-data
ls: /var/lib/docker/volumes/my-data: No such file or directory
```