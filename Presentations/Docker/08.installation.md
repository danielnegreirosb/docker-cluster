# Docker installation

I'm going to setup my environment with Vagrant. 
But tou can use VMs from scratch if you like.

In this scenario we have 3 VMs. docker01 will be master.
And docker02 and docker03 will be workers. They are all using centos 7.

Name it Vagrantfile

```
nodes = [
  { :hostname => 'docker01',  :ip => '192.168.10.10', :ram => 1024, :box => 'centos/7', :disk => '10G' } ,
  { :hostname => 'docker02',  :ip => '192.168.10.20', :ram => 1024, :box => 'centos/7', :disk => '10G' } ,
  { :hostname => 'docker03',  :ip => '192.168.10.30', :ram => 1024, :box => 'centos/7', :disk => '10G' } 
]

Vagrant.configure("2") do |config|
  nodes.each do |node|
    config.vm.define node[:hostname] do |nodeconfig|
      nodeconfig.vm.box = node[:box]
      nodeconfig.vm.hostname = node[:hostname]
      nodeconfig.vm.network :private_network, ip: node[:ip]
      nodeconfig.vm.provider :libvirt do |domain|
        domain.memory = node[:ram]
        domain.cpus = 2
        domain.nested = true
        domain.cpu_mode = 'host-passthrough'
        domain.volume_cache = 'none'
        #domain.storage :file, :size => node[:disk]
      end
    end
  end
end
```

Start up VMs with vagrant in the same directory you created the Vagrantfile
```
vagrant up
```

Now we have access to the 3 VMs

![installation-access.png](images/installation-access.png)


Now edit /etc/hosts, so all hosts know others hosts.
All the steps below are to be executed on all hosts

Become root
```
sudo -i 
```

Manage /etc/hosts
```
cat << EOF >> /etc/hosts
192.168.10.10 docker01
192.168.10.20 docker02
192.168.10.30 docker03
EOF
```

Disable Swap
```
swapoff -a
sed -i.bak '/ swap / s/^(.*)$/#1/g' /etc/fstab
```

Disable SELinux
```
setenforce 0
sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
```

Enable br_netfilter Kernel Module
```
modprobe br_netfilter
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
```

Add repo and install Docker
```
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce
```

Enable services to autostart
```
systemctl start docker && systemctl enable docker
```

Be sure to add exceptions

If you are using Iptables:
```
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -A INPUT -p tcp --dport 2376 -j ACCEPT
iptables -A INPUT -p tcp --dport 2377 -j ACCEPT
iptables -A INPUT -p tcp --dport 7946 -j ACCEPT
iptables -A INPUT -p udp --dport 7946 -j ACCEPT
iptables -A INPUT -p udp --dport 4789 -j ACCEPT
iptables-save
```

If you are using firewall

Firewalld-CMD:
```
firewall-cmd  --add-port=22/tcp --permanent
firewall-cmd  --add-port=2376/tcp --permanent
firewall-cmd  --add-port=2377/tcp --permanent
firewall-cmd  --add-port=7946/tcp --permanent
firewall-cmd  --add-port=7946/udp --permanent
firewall-cmd  --add-port=4789/udp --permanent
```


Reboot

```
reboot
```

Give user permissions on all hosts
```
sudo chmod 0666 /var/run/docker.sock
sudo usermod -aG docker $USER
```

Now only on one is going to be the master node
```
docker swarm init --advertise-addr 192.168.10.10
Swarm initialized: current node (kss78y80zwejfbd4dfnjxdzho) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-22ai56apx7bqchci721ejq6rvne1lfuyxac22kr93biep3otje-0wlxrp62m0hbjyqp4rbnwsmb9 192.168.10.10:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

Copy the docker swarm join --token line to the end. 
And execute it on the worker nodes

```
docker swarm join --token SWMTKN-1-22ai56apx7bqchci721ejq6rvne1lfuyxac22kr93biep3otje-0wlxrp62m0hbjyqp4rbnwsmb9 192.168.10.10:2377

This node joined a swarm as a worker.
```

On The master node, check cluster status

```
docker node ls

ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS      ENGINE VERSION
kss78y80zwejfbd4dfnjxdzho *   docker01            Ready               Active              Leader              19.03.13
k2qrhnh5bz3lrnzpznm1xyz3n     docker02            Ready               Active                                  19.03.13
bg4p5qs8q04cxc7n2u9xwv5qo     docker03            Ready               Active                                  19.03.13

```

Lets make a quick test, to check if its working

Create nginx container and scale it to 6 replicas

```
docker service create --name webapp nginx 
gh6wdlotjoa8xomt5g0smdnkm
overall progress: 1 out of 1 tasks 
1/1: running   [==================================================>] 
verify: Service converged 


docker service scale webapp=6
webapp scaled to 6
overall progress: 6 out of 6 tasks 
1/6: running   [==================================================>] 
2/6: running   [==================================================>] 
3/6: running   [==================================================>] 
4/6: running   [==================================================>] 
5/6: running   [==================================================>] 
6/6: running   [==================================================>] 
verify: Service converged 
```

Now check if its distributed across all servers.
```
docker node ps $(docker node ls -q) --filter desired-state=Running | uniq

ID                  NAME                IMAGE               NODE                DESIRED STATE       CURRENT STATE                ERROR               PORTS
qz4814x6u29h         \_ webapp.1        nginx:latest        docker01            Running             Running about a minute ago                       
l3jmyz3qg9kq         \_ webapp.2        nginx:latest        docker01            Running             Running about a minute ago                       
53pkwlipojqc        webapp.3            nginx:latest        docker03            Running             Running about a minute ago                       
jpummmnhvz1c        webapp.4            nginx:latest        docker03            Running             Running about a minute ago                       
h1r8fwe7tu8a         \_ webapp.5        nginx:latest        docker02            Running             Running about a minute ago                       
tqhsbyyllqmo         \_ webapp.6        nginx:latest        docker02            Running             Running about a minute ago 
```