# Docker-cluster

#### Intro


The goal is quickly set-up a Docker cluster on Linux or Windows with Vagrant and Virtual Box.

With current configuration it creates a cluster with this set-up

| Node | Function | Access 1 | Access 2 |  Memory | vCpu | User | Passwd | 
| :---: | :---: | :---: |  :---: | :---: | :---: | :---: | :---: | 
| master   | master | 192.168.60.10 |  127.0.0.1:2000 | 2048  | 2 | vagrant | test123 | 
| worker01 | worker | 192.168.60.20 |  127.0.0.1:2001 | 1024 | 2 | vagrant | test123 | 
| worker02 | worker | 192.168.60.30 |  127.0.0.1:2002 | 1024 | 2 | vagrant | test123 | 

This configuration can be easily changed in the scripts and Vagrant configuration file.

The set-up comes with:

- Container Engine: Docker

#### Installation

- Install Vagrant
- Install VirtualBox
- Download or git clone and then start VMs

```
$ vagrant up
```

- [Installation Procedure in details](Presentations/Installation.md)


- To stop/start Virtual Machines with vagrant

```
$ vagrant halt
$ vagrant up
```


- To clean up and destroy everything

```
$ vagrant destroy -f
```

## Topics

- Under development. Items are constantly being added or updated.
- If you are not familiarized with Docker. Please see at the end of summary the Docker review first.

### Docker Basic Review
  
   
- [What is Container](Presentations/Docker/01.what-is-container.md)
- [Images](Presentations/Docker/02.images.md)
- [Containers](Presentations/Docker/03.containers.md)
- [Networking](Presentations/Docker/04.networking.md)
- [Storage](Presentations/Docker/05.storage.md)
- [Registry](Presentations/Docker/06.registry.md)
- [Docker Orchestration with Swarm](Presentations/Docker/07.docker-orchestration-swarm.md)
- [Installation](Presentations/Docker/08.installation.md)

<br />

[Any questions, suggestions, please reach](danielnegreirosb@gmail.com) danielnegreirosb@gmail.com